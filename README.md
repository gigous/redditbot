To use this bot, install python 3 (3.5.2+ recommended) and the following python modules:

## Instructions for Debian Linux

    ` sudo apt-get install python3 python3-pip`

praw - Python Reddit API Wrapper
    ` pip3 install --user --no-cache praw`

bs4 - Beautiful Soup 4
    ` pip3 install --user --no-cache beautifulsoup4`

yaml - YAML parsing
    ` pip3 install --user --no-cache setuptools PyYaml`
