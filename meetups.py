from WolfPACBot import WolfPACBot
import datetime
import time

if __name__ == "__main__":
    bot = WolfPACBot()
    if bot.login():
        while True:
            wed = bot.find_upcoming_day_of_week(2)
            wed_at_10 = datetime.datetime(wed.year, wed.month, wed.day, 10, 0) # 10:00
            time_until_post = wed_at_10 - datetime.datetime.now()
            tot_secs = time_until_post.total_seconds()
            now = datetime.datetime.now()
            print("Gonna sleep for %s seconds starting at %s\nMeetups post will occur on %s" % (tot_secs, now, now + datetime.timedelta(seconds=tot_secs)))
            time.sleep(tot_secs)
            bot.submit_meetups_post(subreddit="WolfPACtest")
