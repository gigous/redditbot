import sys
import params # specific to this application
import praw
from prawcore import OAuthException
from bs4 import BeautifulSoup
import requests
import datetime
import yaml
import time
import logging

# TODO: make private or internal
class FIELD:
    NAME =      "©name©"
    LINK =      "©link©"
    CITY =      "©city©"
    STATE =     "©state©"
    STATES =    "©states©"
    WEEKDAY =   "©weekday©"
    MONTH =     "©month©"
    DAY =       "©day©"
    YEAR =      "©year©"
    TIME =      "©time©"
    ZONE =      "©zone©"
    LOCATION =  "©location©"

# TODO: make private or internal
class STATES:
    abbr = {
        'AK': 'Alaska', 'AL': 'Alabama', 'AR': 'Arkansas', 'AZ': 'Arizona', 'CA': 'California',
        'CO': 'Colorado', 'CT': 'Connecticut', 'DE': 'Delaware', 'FL': 'Florida', 'GA': 'Georgia',
        'HI': 'Hawaii', 'IA': 'Iowa', 'ID': 'Idaho', 'IL': 'Illinois', 'IN': 'Indiana',
        'KS': 'Kansas', 'KY': 'Kentucky', 'LA': 'Louisiana', 'MA': 'Massachusetts', 'ME': 'Maine',
        'MD': 'Maryland', 'MI': 'Michigan', 'MN': 'Minnesota', 'MO': 'Missouri', 
        'MS': 'Mississippi', 'MT': 'Montana', 'NC': 'North Carolina', 'ND': 'North Dakota',
        'NE': 'Nebraska', 'NH': 'New Hampshire', 'NJ': 'New Jersey', 'NM': 'New Mexico', 
        'NV': 'Nevada', 'NY': 'New York', 'OH': 'Ohio', 'OK': 'Oklahoma', 'OR': 'Oregon',
        'PA': 'Pennsylvania', 'RI': 'Rhode Island', 'SC': 'South Carolina', 'SD': 'South Dakota',
        'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah', 'VT': 'Vermont', 'VA': 'Virginia', 
        'WA': 'Washington', 'WI': 'Wisconsin', 'WV': 'West Virginia', 'WY': 'Wyoming'
    }
    utc_offset = {
        'AK': '0900', 'AL': '0600', 'AR': '0600', 'AZ': '0700', 'CA': '0800',
        'CO': '0700', 'CT': '0500', 'DE': '0500', 'FL': '0500', 'GA': '0500',
        'HI': '1000', 'IA': '0600', 'ID': '0700', 'IL': '0600', 'IN': '0500',
        'KS': '0600', 'KY': '0500', 'LA': '0600', 'MA': '0500', 'ME': '0500',
        'MD': '0500', 'MI': '0500', 'MN': '0600', 'MO': '0600', 'MS': '0600', 
        'MT': '0700', 'NC': '0500', 'ND': '0600', 'NE': '0600', 'NH': '0500', 
        'NJ': '0500', 'NM': '0700', 'NV': '0800', 'NY': '0500', 'OH': '0500',
        'OK': '0600', 'OR': '0800', 'PA': '0500', 'RI': '0500', 'SC': '0500', 
        'SD': '0600', 'TN': '0600', 'TX': '0600', 'UT': '0700', 'VT': '0500',
        'VA': '0500', 'WA': '0800', 'WI': '0600', 'WV': '0500', 'WY': '0700'
    }
    time_zones = {
        '-1000': 'HST', '-0900': 'AKST', '-0800': 'PST', '-0700': 'MST', '-0600': 'CST', '-0500': 'EST'
    }

# TODO: make private or internal??
class Event:
    def __init__(self):
        self.name        = None # What the event is called
        self.url         = None # Link to the event info
        self.venue       = None # The name of the venue, where event is held
        self.city        = None #
        self.state       = None #
        self.time        = None # Date and time of event, as datetime
        self.location    = None # Link to location on Google Maps

    def __str__(self):
        return self.name + " will be held in " + self.city + ", " + self.state + " at " + self.venue + ". " + \
            "It will occur on " + str(self.time)

class WolfPACBot:

    # DONE: Make an event class? Named tuple?
    # TODO: Make the Sunday a member? (wtf did I mean)

    def __init__(self, logFilename=None, logLevel = logging.DEBUG):
        self.bot    =       None  # The reddit object
        self.events =       None  # List of Events
        self.yaml   =       None  # yaml parsing object thing
        self.discuss_post = None  # Last discussion post submitted
        self.meetups_post = None  # Last meetups post submitted
        # TODO: Save url of posts if bot needs to recover?

        if logFilename:
            logging.basicConfig(filename=logFilename, level=logging.DEBUG)

    def login(self):
        """
        Log the bot into Reddit so it can make posts and stuff
        """
    
        self.bot = praw.Reddit(
            client_id=params.client_id,
            client_secret=params.client_secret,
            username=params.username,
            password=params.password,
            user_agent=params.user_agent
            )

        try:
            logging.info("Logged in as " + str(self.bot.user.me()))
            return True
        except OAuthException as e:
            print(e)
            logging.critical("Could not log in as WolfPACBot. Authenication failed.");
            return False
        except:
            print(sys.exc_info())
            logging.critical("Could not log in as WolfPACBot. I don't know what happened, see above.")
            return False


    def _write_discuss_post_title(self):
        assert self.yaml, "Don't have yaml parser!"
        post_title = self.yaml['discuss_title']
        
        today = datetime.date.today()
        month_value = today.strftime("%B")
        day_value = str(int(today.strftime("%d")))
        year_value = today.strftime("%Y")

        post_title =  post_title.replace(FIELD.MONTH, month_value)      \
                                .replace(FIELD.DAY, day_value)          \
                                .replace(FIELD.YEAR, year_value)

        return post_title


    def _write_discuss_post_text(self):
        assert self.yaml, "Don't have yaml parser!"
        post_text = self.yaml['discuss_main_text']
        post_text += "\n\n" + self.yaml['signature']
        return post_text


    def submit_discuss_post(self, subreddit="WolfPAChq", dry_run=False):
        # TODO: unsticky old post and sticky new post after 1 day
        # TODO: Add flair Q&A
        # Get most recent post info

        with open("post_info.yaml", "r") as file_obj:
            self.yaml = yaml.load(file_obj)

        post_title = self._write_discuss_post_title()
        post_text = self._write_discuss_post_text()

        with open("post.txt", "w") as file_obj:
            file_obj.write(post_title + "\n")
            file_obj.write(post_text)

        if not dry_run:
            logging.debug("Not a dry run, submitting discussion post...")
            sr = self.bot.subreddit(subreddit)
            self.discuss_post = sr.submit(post_title, post_text)
            logging.debug("Discussion post submitted to r/%s", subreddit)

    def sticky_last_discuss_post(self):
        if not self.discuss_post:
            logging.error("No discussion post submitted (in this bot's lifetime)!")
            return

        self.discuss_post.mod.sticky()
        logging.debug('Discussion post stickied: "%s', self.discuss_post.title)
    
    def get_events(self):
        """
        Scrape data about meetups from Wolf-PAC's website
        """
        r = requests.get(params.events_page)
        soup = BeautifulSoup(r.text, 'html.parser')
        
        divs = soup.find_all('div') # get all divs in page
        cl = [div.get('class') for div in divs] # get all divs' classes
        ev_info_idx =   [i for i in range(len(cl)) if cl[i] == ['event-info']] # indices of event-info divs
        ev_venue_idx =  [i for i in range(len(cl)) if cl[i] == ['event-venue']] # indices of event-venue divs
        ev_info_tags =  [divs[x] for x in ev_info_idx] # get all event-info tags
        ev_venue_tags = [divs[x] for x in ev_venue_idx] # get all event-venue tags

        self.events = [] # erase list, we'll get up-to-date info
        for event_info, event_venue in zip(ev_info_tags, ev_venue_tags):
            event = Event() # Create new Event instance

            citystate = event_venue.get_text() # which city and state the event is in
            if not citystate: # is there text in this <div>?
                str_err = "Somehow we have an event-venue class, but no content..."
                logging.error(str_err)
                raise Exception(str_err)
            # We'll do something with it shortly...

            if not event_venue.a: # do we have <a> tag?
                str_err = "Expected <a> tag for venue name and location link, didn't find tag."
                logging.error(str_err)
                raise Exception(str_err)
            event.venue = event_venue.a.get_text() # Where the event is
            event.location = event_venue.a.get("href") # Google Maps link

            citystate = " ".join(citystate.split())[len(event.venue)+len(" in "):] # remove redundancy
            event.city = citystate[:-4]     # get city name
            event.state = citystate[-2:]    # get state abbreviation

            if not event_info.h4:
                str_err = "Expected <h4> tag for the event name, didn't find tag."
                logging.error(str_err)
                raise Exception(str_err)
            event.name = event_info.h4.get_text()
            if not event_info.a:
                str_err = "Expected <a> tag for the event URL, didn't find tag."
                logging.error(str_err)
                raise Exception(str_err)
            event.url = 'http://wolf-pac.com' + event_info.a.get('href')

            ei_div = event_info.find_all('div')
            event_deds = [ed for ed in ei_div if ed.get('id') == 'event-deds'][0] # only one event-deds
            if not event_deds:
                str_err = "Couldn't find event-deds class, need it for time."
                logging.error(str_err)
                raise Exception(str_err)
            if not event_deds.strong:
                str_err = "Expected <strong> tag for event time."
                logging.error(str_err)
                raise Exception(str_err)
            day_and_time = event_deds.strong.get_text() # day and time as string with some string junk
            janet = day_and_time.split() # janet is our list of string tokens
            # if janet has ·, we want to remove that cancer from her. Otherwise, we love her just as she is
            janet = janet[0:janet.index('·')] if '·' in janet else janet
            event_time_str = " ".join(janet)        # join string tokens back together
            try:
                tz = STATES.utc_offset[event.state]     # get offset from UTC for timezone
            except KeyError as e:
                logging.error(event.state + " is not a valid state abbreviation. Unless recently added to union...")
                logging.info("This event will not be announced: %s", str(event))
                print(e)
                continue
            event_time_str += " -" + tz             # add it to time string
            event.time = datetime.datetime.strptime(event_time_str, '%A, %B %d, %Y at %I:%M %p %z')
                
            self.events.append(event)
            logging.debug("Appended event: %s", str(event))


    def find_upcoming_day_of_week(self, weekday):
        """
        Return next date of a specific weekday. (Ex: When is next Monday?)

        Keyword arguments:
        weekday -- day of the week specified as an integer (0 is Monday)
        """
        day_of_week = datetime.date.today() + datetime.timedelta(1)
        while day_of_week.weekday() is not weekday: # Sunday
            day_of_week += datetime.timedelta(1) # advance one day
        return day_of_week


    def events_for_week(self):
        week_events = []
        # go through events list until an event is found that is later than NEXT saturday
        # (use %w for weekday, 0=sunday 6=saturday)
        sunday = self.find_upcoming_day_of_week(6)
        next_saturday = sunday + datetime.timedelta(6)

        for event in self.events:
            if event.time.date() < sunday or event.time.date() > next_saturday: # not held from Sun to Sat
                continue
            week_events.append(event)
        return week_events


    def _write_post_text(self, events_next_week):
        assert self.yaml, "Don't have yaml parser!"
        post_text = ""
        for event in events_next_week:
            row = self.yaml['event_table_row']

            month_value = event.time.strftime("%B")
            day_value = str(int(event.time.strftime("%d")))
            weekday_value = event.time.strftime("%A")
            year_value = event.time.strftime("%Y")
            hour = str(int(event.time.strftime("%I")))
            minute_am_pm = event.time.strftime(":%M%p")
            time_value = hour + minute_am_pm
            zone_value = STATES.time_zones[event.time.strftime("%z")]
            
            row =    row.replace(FIELD.NAME, event.name)        \
                        .replace(FIELD.LINK, event.url)         \
                        .replace(FIELD.CITY, event.city)        \
                        .replace(FIELD.STATE, event.state)      \
                        .replace(FIELD.WEEKDAY, weekday_value)  \
                        .replace(FIELD.MONTH, month_value)      \
                        .replace(FIELD.DAY, day_value)          \
                        .replace(FIELD.TIME, time_value)        \
                        .replace(FIELD.ZONE, zone_value)        \
                        .replace(FIELD.LOCATION, event.location)
        
            post_text += "\n" + row
            
        post_text += "\n\n" + self.yaml['signature']
        return post_text


    def _write_meetups_post_title(self, events_next_week):
        assert self.yaml, "Don't have yaml parser!"
        post_title = self.yaml['meetups_title']
        
        # Get state names with meetups this week
        states_with_events = []
        for event in events_next_week:
            ST = event.state
            try:
                state_name = STATES.abbr[ST] # get full state name
            except KeyError as e: 
                logging.error("State not found: " + STATES.abbr[ST])
                logging.info("Event not posted: %s", str(event))
                continue
            
            states_with_events.append(state_name)

        # Remove any duplicates and sort by name
        states_with_events = list(set(states_with_events))
        states_with_events.sort()

        states_value = ""
        for state_name in states_with_events:
            states_value += state_name + ', '
        states_value = states_value[:-2] # get rid of extra comma

        # Get date for week
        sunday = self.find_upcoming_day_of_week(6)
        month_value = sunday.strftime("%B")
        day_value = str(int(sunday.strftime("%d")))
        year_value = sunday.strftime("%Y")

        post_title =  post_title.replace(FIELD.MONTH, month_value)      \
                                .replace(FIELD.DAY, day_value)          \
                                .replace(FIELD.YEAR, year_value)        \
                                .replace(FIELD.STATES, states_value)

        return post_title


    def _write_meetups_post_text(self, events_next_week):
        assert self.yaml, "Don't have yaml parser!"
        post_text = self.yaml['meetups_main_text'] + "\n" + self.yaml['event_table_header']
        post_text = post_text[:-1] # remove mysterious newline
        return post_text + self._write_post_text(events_next_week)


    def submit_meetups_post(self, subreddit="WolfPAChq", dry_run=False):
        self.get_events()
        events_next_week = self.events_for_week()
        if not events_next_week:
            # DONE: Turn these into log messages
            logging.info("No events for the week...")
            return

        # get most recent version of yaml file
        with open("post_info.yaml", "r") as file_obj:
            self.yaml = yaml.load(file_obj)

        post_title = self._write_meetups_post_title(events_next_week)
        post_text = self._write_meetups_post_text(events_next_week)

        with open("post.txt", "w") as file_obj:
            file_obj.write(post_title + "\n")
            file_obj.write(post_text)
        
        if not dry_run:
            sr = self.bot.subreddit(subreddit)
            sr.submit(post_title, post_text)


    def _write_reminder_post_title(self, events):
        post_title = self.yaml['reminder_title']
        
        # Get state names with meetups this week
        states_all = []
        for event in events:
            ST = event[3]
            try:
                state_name = STATES.abbr[ST]
            except KeyError as e: 
                logging.error("State not found: " + STATES.abbr[ST])
                logging.info("Event not posted: %s", str(event))
                continue
            
            states_all.append(state_name)

        states_all = list(set(states_all))
        states_all.sort()

        states_value = ""
        for state_name in states_all:
            states_value += state_name + ', '
        states_value = states_value[:-2] # get rid of extra comma

        post_title =  post_title.replace(FIELD.STATES, states_value)

        return post_title


    def _write_reminder_post_text(self, events):
        post_text = self.yaml['reminder_main_text'] + "\n" + self.yaml['event_table_header']
        post_text = post_text[:-1] # remove mysterious newline
        return post_text + self._write_post_text(events_next_week)


    def submit_reminder_post(self, events):
        return # May not use
        if not events:
            logging.error("No events to remind people about, wtf...")
            return

        # get most recent version of yaml file
        with open("post_info.yaml", "r") as file_obj:
            self.yaml = yaml.load(file_obj)

        post_title = self._write_reminder_post_title(events_next_week)
        post_text = self._write_reminder_post_text(events_next_week)

        with open("post.txt", "w") as file_obj:
            file_obj.write(post_title + "\n")
            file_obj.write(post_text)
        
        sr = self.bot.subreddit("WolfPACtest")
        sr.submit(post_title, post_text)

        
    def poll_for_reminder(self):
        if not self.events:
            self.get_events()
        e = list(self.events)
        t = datetime.datetime.now()
        
        while True:
            wed = self.find_upcoming_day_of_week(2)
            upcoming_wed_at_10 = datetime.datetime(wed.year, wed.month, wed.day, 10, 0) # 10:00
            time_until_meetups_post = upcoming_wed_at_10 - datetime.datetime.today()
            if e:
                first_event = e[0][4]
                day_before_meetup_at_10 = datetime.datetime(first_event.year, first_event.month, first_event.day, 10, 0) # 10:00
                time_until_reminder_post = day_before_meetup_at_10 - datetime.datetime.today()
            else:
                time_until_reminder_post = datetime.datetime(3000, 12, 31) # never

#            if time_until_reminder_post
            print(time_until_meetups_post)
            print(time_until_reminder_post)
            time.sleep(1)

            self.get_events()
            e = list(self.events)
            break
            
#               self.events.append((event_name, event_page, city, ST, event_date))

if __name__ == '__main__':

    bot = WolfPACBot()
    bot.login()
    bot.loop()
    #bot.get_events()
    #events_next_week = bot.events_for_week()
    #[print(event) for event in events_next_week]

    #bot.poll_for_reminder()

    
    
    #print(bot._write_meetups_post_title(events_next_week))
    #bot.submit_reminder_post(events_next_week)
