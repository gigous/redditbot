from WolfPACBot import WolfPACBot
import datetime
import time

if __name__ == "__main__":
    bot = WolfPACBot("discuss.log")
    bot.login()
    while True:
        mon = bot.find_upcoming_day_of_week(0)
        mon_at_10 = datetime.datetime(mon.year, mon.month, mon.day, 10, 0) # 10:00
        time_until_post = mon_at_10 - datetime.datetime.now()
        tot_secs = time_until_post.total_seconds()
        now = datetime.datetime.now()
        print("Gonna sleep for %s seconds starting at %s\nPost will occur on %s" % (tot_secs, now, now + datetime.timedelta(seconds=tot_secs)))
        time.sleep(tot_secs)
        bot.submit_discuss_post()
        print("Posted!")
        print("Post will be stickied in 24 hours...")
        time.sleep(86400)
        bot.sticky_last_discuss_post()
        print("Post stickied.")
